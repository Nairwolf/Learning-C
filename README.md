# LEARNING-C

## Objective

Small courses about C language in order to learn basic features. This project is based
on the white paper [Le C en 20 heures](http://framabook.org/le-c-en-20-heures-2/). It's 
supposed to be for someone who don't know anything in programmation, so for a real 
beginners. We will try to make these courses as easy as possible. 

## Advice

There is several examples of C code. Firsty, it's better to try to make exercices without
seeing correction. But if you feel you need to watch correction, try to write by yourself
this code, and do not use copy and paste. If you want to learn C, and be familiar with 
its syntax, it's always better to re-write a code source you want to learn.  

## Summary 

* [Lesson 1](https://gitlab.com/Nairwolf/Learning-C/blob/master/lesson-1/README.md)
