# Leçon 1 - Premiers pas

## Objectif

* Se familiariser avec les outils de base pour un développeur (terminal, compilateur, 
editeur de texte). 
* Être capable de compiler son premier programme C.

## Pré-requis

* Un système d'exploitation basé sur Unix (pour des débutants, une distribution dérivée 
d'Ubuntu fera très bien l'affaire). Il est évidemment pas obligatoire d'être sur Unix 
pour développer en C, mais c'est souvent l'environnement favori des développeurs C. 

## 1. Langage de programmation

### Qu'est-ce qu'un langage de programmation ? 

Un langage de programmation, c'est une sorte de *langue* spéciale qu'un développeur écrit
pour pouvoir être compris par un ordinateur. Il existe une multitude de langages de 
programmation, tellement qu'il est impossible d'en savoir le nombre exact. Certains de 
ses langages sont très très utilisés, d'autres beaucoup moins. Nous allons étudier le 
[langage C](https://fr.wikipedia.org/wiki/C_%28langage%29) inventé en 1970, et qui est 
un des langages les plus utilisés au monde. 

Un langage de programmation c'est une suite d'instructions que l'ordinateur va pouvoir
comprendre. Il ne s'agit donc que du *texte*.

### Que peut-on faire avec un langage de programmation ? 

Tout ce qui existe sur votre ordinateur a été écrit par un langage de programmation, que
ce soit votre navigateur internet, votre logiciel de lecture de CD/DVD, l'explorateur 
de fichiers. Mais aussi, des logiciels que l'on voit *moins* comme des logiciels qui 
gèrent la mémoire de votre disque dur, et les fichiers écrits dedans. Quand vous copiez,
ou enregistrez un fichier sur votre ordinateur, un logiciel est chargé de faire cela et
etc. Même les systèmes d'exploitations, comme Windows, MacOs ou GNU/Linux sont aussi 
écrits avec un langage de programmation. Et ils sont écrits en C majoritairement ! C'est
une des raisons, que nous allons étudier aujourd'hui, le langage C. 

## 2. Les outils d'un développeur 

### Qu'ai-je besoin pour programmer ? 

Je l'ai dit, un programme ce n'est que du *texte*. Il nous faudra donc un logiciel qui
nous permet d'écrire du texte, donc un 
[éditeur de texte](https://fr.wikipedia.org/wiki/%C3%89diteur_de_texte). Sous GNU/Linux,
nous pouvons utiliser [Gedit](https://wiki.gnome.org/Apps/Gedit). Le code écrit par un 
développeur étant simplement du *texte* il n'est pas directement compris par votre 
ordinateur. En effet, un ordinateur ne comprends qu'une suite de 1 ou de 0. Il nous faut
donc utiliser ce qu'on appelle un *compilateur*. Il s'agit d'un outil qui va transformer
le code source en code binaire (une suite de 1 et de 0) pour que votre ordinateur puisse
le comprendre. Nous allons utiliser le célèbre compilateur 
[GCC](https://fr.wikipedia.org/wiki/GNU_Compiler_Collection). 

### Comment me procurérer ces outils ? 

Sur une distribution basé sur Ubuntu, il faut exécuter ces commandes sur un *terminal*. 

* Pour installer Gedit :

	`sudo apt install gedit`

* Pour installer Gcc : 

	`sudo apt install build-essential`

### Le terminal 

#### Tu viens de parler d'un terminal, qu'est-ce que c'est ? 

Ce que l'on appelle *terminal* a plusieurs noms. Certaines personnes appellent cela un 
*terminal de controle*, ou un *invite de commande*, une *console* ou encore un *shell*.
Il faut remonter au début de l'informatique. Il n'y avait pas d'interface graphique, il 
n'était pas possible de cliquer sur une icône pour lancer une application. Il n'y avait 
même pas de souris. La seule chose dont disposait les utilisateurs, c'était un 
*terminal de controle*, ou *invite de commande*. Un terminal est un point d'accès de 
communication entre l'utilisateur et la machine. Le nom *invite de commande* est très
parlant. Cela signifie qu'un terminal, *invite l'utilisateur a tapper des commandes* pour
la machine. Sur un terminal, il est donc possible de *contrôler* un ordinateur, et de lui
demander d'exécuter des actions. 

#### Pourquoi c'est utile ? 

C'est utile car beaucoup de programme n'ont pas d'interface graphique, et n'en ont pas 
besoin. **GCC** par exemple s'utilise sans interface graphique. Il faudra compiler votre
programme directement depuis le terminal. 

Le terminal nous a aussi permis d'installer des *paquets* très facilement (Dans le monde 
Unix, on n'installe pas un logiciel, mais des paquets). Expliquons la commande que nous
avons tapé : `sudo apt install gedit`.

* **sudo** : permet de demander à l'ordinateur les droits d'administrateur pour faire une 
action particulière. Un simple utilisateur a certains droits sur un ordinateur, mais 
un administrateur a des droits plus élevés. Si vous avez votre propre machine, vous êtes
aussi l'administrateur de votre machine. Mais sur un ordinateur publique, celui de votre 
école, ou d'une bibliothèque par exemple, un simple utilisateur n'a normalement pas le 
droit d'installer de nouveaux paquets pour éviter qu'il installe des virus, par exemple. 
Si votre école vous autorise à installer des logiciels sur un ordinateur publique, il 
n'est pas sécurisé !  

* **apt** : c'est le nom de la commande que l'on demande d'excécuter. apt est un 
gestionnaire de paquets. Il gère les paquets installés sur votre machine, et si vous lui
demandez d'installer un paquet, il sait où aller télécharger le paquet, et comment 
l'installer sur votre machine. 

* **install** : c'est une commande spéciale que vous demandez à apt. Apt peut faire 
plein de choses, par exemple effacer un paquet, ou mettre à jour vos paquets, et là, vous
lui demandez d'installer quelque chose. 

* **gedit** : c'est le nom du paquet que vous demandez d'installer. apt va trouver
tout seul où télécharger ce fichier, et va prendre directement la bonne version pour 
votre machine. C'est pas beau la vie ? 

#### Principales commandes sur un terminal Unix

Il n'est pas nécessaire d'apprendre tout ce que l'on peut faire avec un terminal pour 
pouvoir programmer, mais il est nécessaire de connaître quelques bases, et quelques
commandes essentielles. 

Un terminal se présente de la façon suivante : 
`your_name@name_of_your_computer:your_location$`

Si vous voyez le signe `$` cela signifie que le terminal est prêt à prendre en compte
votre commande. 

Entre le `:` et le `$`, le terminal vous indique à quel endroit vous 
êtes. Lorsque vous l'ouvrez pour la première fois, il vous indique `~`, il s'agit d'un 
raccourci pour dire `/home/your_name`. Unix organise ses fichiers selon une certaine
arborescence. Tout commence par la **racine** que l'on note `/`. A cette racine, il y a 
un certain nombre de répertoires, comme par exemple `/bin` ou `/dev`. Par défaut, quand 
vous ouvrez un terminal, vous vous trouvez dans votre **home directory** (répertoire 
maison), qui correspond à l'addresse `/home/your_name`. C'est généralement ici que l'on 
stocke ses fichiers personnels (documents, images, vidéos). Comme c'est un emplacement
très utilisé, un raccourci a été fait. Si vous voyez le signe `~`, cela fait référence à 
`/home/your_name`. Par exemple l'addresse `~/Documents/` fait référence à l'addresse 
`/home/your_name/Documents/`. Cela signifie que *depuis la racine*, il faut aller sur le
répertoire `home`, puis le répertoire `documents`. Vous trouverez plus d'information sur
l'arborescence Unix sur la 
[documentation francophone d'Ubuntu](http://doc.ubuntu-fr.org/arborescence).

* **pwd** : Permet de connaître son emplacement. En anglais, cela signifie "**p**rint 
**w**orking **d**irectory". 

* **ls** : Permet d'afficher les fichiers et répertoires présents à notre emplacement.
En anglais, cela signifie "**l**i**s**t directory contents". 

* **cd** : Permet de se déplacer l'arborescence des fichiers. Il y a deux moyens de se 
déplacer dans l'arborescence. 

	* Par chemin relatif : C'est à dire que vous prenez en compte l'endroit où vous êtes
	, et vous écrivez l'addresse du chemin où vous voulez aller. Par exemple, vous êtes 
	sur votre **home directory**, et vous voulez vous rendrez dans le répertoire `lycée` 
	qui est présent dans le répertoire `Documents`. Pour y accéder, faites 
	`cd Documents/lycée`. 

	* Par chemin absolu : Cela signifie que lorsque vous écrivez l'endroit à laquel vous
	voulez accéder, il faut écrire l'addresse absolu, *depuis la racine*. L'avantage, 
	c'est que vous n'avez pas à vous souciez de l'endroit où vous êtes puisque vous 
	écrivez l'addresse *exacte* du répertoire que vous voulez accéder. Par exemple, pour 
	le dossier `lycée`, faites `cd /home/your_name/Documents/lycée`. C'est toutefois plus
	long. Comme on l'a vu, utilisez le raccourci `~` pour remplacer `/home/your_name`. 
	Cela devient : `cd ~/Documents/lycée`. 

#### Comment taper la commande XXX ?

Si vous souhaitez obtenir des informations sur une commande, il suffit d'appeler **man**.
Il s'agit du Manuel dans lequel est documenté la plupart des commandes. Essayez de faire
`man ls` pour avoir la documentation de `ls` par exemple. (aide : pour quitter le manuel,
on vous aide en disant de taper "q" pour quitter). De manière générale, lorsqu'on tape 
une commande sur un terminal, cela se passe de la façon suivante : 

`nom_de_la_commande -option1 -option2 argument1 argument2`

##### Options
Une commande peut être exécuté avec des options particulières, et 
généralement optionnelles. Ces options sont différenciés des arguments par le symbole `-`
devant le nom de l'option. Il faut lire la documentation (man) pour avoir la liste de 
toutes les options. Pour la commande `ls`, essayez de voir ce que donne `ls -l` 
(option -l), en l'excécutant sur un terminal, mais en lisant aussi la documentation.

##### Arguments
Une commande s'exécute souvent sur un fichier particulier, ou un répertoire particulier. 
C'est ce que l'on appelle l'argument de la commande. Certaines commande prennent 
plusieurs arguments, d'autres commandes ne prennent qu'un seul argument, pour d'autres, 
l'argument est optionel. `cd` est par exemple une commande qui prend en argument 
l'addresse que vous voulez rejoindre.  

## 3. Mon premier programme

Nous allons commencer véritablement à coder. Ouvrez Gedit, et écrivez le texte suivant : 

	main() {
		puts ("Bonjour");
	}

Remarquez la tabulation à la ligne 2 et 3. Bien qu'elles ne soient pas nécessaire, elles
permettent de mettre de l'ordre dans le code, et de le rendre plus visible. Un conseil 
très important et de ne pas faire de copier coller, mais d'écrire complètement le code, 
pour pouvoir mémoriser au maximum la syntaxe et la logique. 

Vous enregistrez ce fichier (CTRL+s) sous le nom `programme1.c`. 

Il faut maintenant convertir le code source que l'on vient d'écrire en binaire. On va 
utiliser **GCC**. Tout d'abord, ouvrez un terminal et déplacez-vous dans le fichier
où vous avez enregistré `programme1.c`. (Utilisez la commande `cd nom_du_repertoire` 
pour se déplacer). Vérifiez avec `ls` que le fichier `programme1.c` est bien présent. 
Pour compiler, tapez la commande suivante que nous expliquerons un peu plus tard.

	gcc -o programme1 programme.c. 


Pour exécuter le programme, tapez la commande suivante. Si tout se passe bien, vous 
devrez avoir un "Bonjour" qui s'affiche. 

	./programme1

### Comment compiler un programme avec GCC ?

Arrêtons-nous un instant pour apprendre à compiler un programme. **GCC** est là pour 
cela. Vous pouvez lire la documentation de **GCC** (`man gcc`) mais vous vous rendrez 
compte que c'est un programme compliqué avec beaucoup d'options. Cette commande peut
se découper en 3 parties. 

* **gcc** : C'est le nom de la commande que l'on tape, pour l'appeler. Rien de plus 
normal.
* **-o programme1** : On utilise l'option -o (qui signifie **output**, et **sortie** en 
français). Cela signifie que l'on indique à GCC que le programme que l'on veut créer en 
sortie va s'appeler **programme1**.  
* **programme1.c** : Il n'y a pas d'option avant, cela signifie que c'est un argument
de la commande **gcc**. On indique à **gcc** le programme que l'on souhaite compiler. 

C'est simple n'est-ce pas ? 

Et maintenant, comment on *exécute* un programme ? De la même façon dont on appelle les
autres programmes, en tapant leur nom. Si vous essayez de taper `programme1`, cela ne 
marchera pas. Pourquoi ? Parce que le terminal cherche s'il y a une commande qu'il 
connaît qui s'appelle `programme1`. Il n'en connaît pas, et donc renvoie une erreur (
généralement commande introuvable). Il faut alors dire au terminal **où** se trouve le 
logiciel. Pour cela, on utilise le `.` qui signifie **répertoire courant** (répertoire
actuel). On tape donc la commande `./programme1`, qui signifie "dans le répertoire 
courant, exécute programme1". 

### Un programme sans erreur

**GCC** est un outil très puissant, et peut nous indiquer si nous avons fait des erreurs
dans le code source que nous avons écrit. Pour cela, il faut utiliser l'option `-Wall`. 
Cela signifie "All Warning". Le compilateur va nous indiquer des warnings, donc des 
alertes sur des erreurs potentielles de notre programme. Lancez cette commande : 

	gcc -Wall -o programme1 programme1.c

Le compilateur va vous renvoyer des `Warning`. Ce ne sont pas des erreurs, mais le 
compilateur vous avertit qu'il y a peut-être un problème avec ce que vous avez fait. Vous
pouvez penser que ce n'est pas grave tant que ça marche, mais c'est le meilleur moyen
de faire des bugs plus tard, alors attention ! Nous allons essayer de corriger ces 
warnings. 

Conseil : Le compilateur vous aide en vous disant où se trouve l'erreur. S'il dit 
`programme1.c:1:1: warning: ...` cela signifie que pour le fichier `programme1.c` à la 
ligne 1, à la colonne 1, il y a un warning. 

#### programme1.c:2:2: warning: implicit declaration of function \'puts\'

Pour votre premier programme, vous avez utilisé la fonction `puts()` qui permet 
d'afficher un texte à l'écran. Vous n'avez pas codé cette fonction, vous avez utilisé une
fonction qui est présente dans une bibliothèque de fonctions. Fondamentalement, le 
langage C n'est qu'un nombre restreint d'instructions et un ensemble de bibliothqèues. 
Dans ces dernières, le compilateur trouve les fonctions et les applications qui lui 
permettent de créer un programme exécutable. C'est un peu ce que vous faites lorsque vous
recherchez dans une encyclopédie pour réaliser un exposé. 

Vous avez utilisé la fonction puts qui n'est pas dans votre programme, il faut indiquer
au compilateur où se trouve la bibliothèque et que vous souhaitez utiliser les fonctions
de cette bibliothèque. La fonctions puts se trouve dans la bibliothèque standard 
d'entrées-sorties (qui gère ce que l'utilisateur écrit sur le clavier, et ce qui 
s'affiche sur un écran, par exemple).

Il faut ajouter dans le code la ligne suivante en tout début du programme : 

	#include <stdio.h>

`stdio.h` est le nom de la bibliothèque où se trouve `puts`. std veut dire standard, et 
io signifie input/output (entrée/sortie en français).  

#### programme1.c:3:1: warning: control reaches end of non-void function

L'erreur est un peu plus difficile à comprendre maintenant. Il faut savoir que tout 
programme doit renvoyer une valeur de retour, tout à la fin. Cela permet de savoir si 
le programme s'est excecuté correctement, ou s'il y a eu des erreurs. Lorsque tout va 
bien, on retourne la valeur 0. On doit rajouter la ligne après la fonction `puts` : 

	return 0;

#### Programme sans erreur

Si vous avez corrigé le programme, vous obtenez ceci : 

	#include <stdio.h>
	
	int main() {
		puts("Bonjour");
		return 0;
	}


